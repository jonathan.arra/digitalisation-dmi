##############################################################################
#
# Copyright (c) 2023 - LaConsultanceIT - jonathan.arra@laconsultanceit.com
# Author: Jean Jonathan ARRA
# Fichier du module crm_custom
# ##############################################################################
{
    "name": "Dematerialisation des DMIs",
    "version": "1.0",
    "author": "Jean Jonathan ARRA - La Consultance IT",
    'category': 'Localization',
    "website": "http://www.laconsultanceIT.ci",
    "depends": ["base", "hr", "project"],
    "description": """
    """,
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [

        ],
    "data": [
        "security/res_group.xml",
        "security/ir_rules.xml",
        "security/ir.model.access.csv",
        "data/mail_template_equipement.xml",
        "data/mail_template_flux.xml",
        "data/mail_template_habilitation.xml",
        "data/mail_template_vm.xml",
        "data/sequence_data.xml",
        "data/stage_data.xml",
        "wizards/dmi_refus_wizard_view.xml",
        "wizards/dmi_more_info_wizard_view.xml",
        "wizards/dmi_info_answer_wizard_view.xml",
        "views/dmi_stage_view.xml",
        "views/dmi_setting_view.xml",
        "views/dmi_managnment.xml",
        "views/dmi_managnment_habilitation_view.xml",
        "views/dmi_equipement_view.xml",
        "views/dmi_managnment_flux.xml",
        "views/res_partner_view.xml",
        "views/res_config_settings_views.xml",
        "views/menu_item_view.xml"
    ],
    "installable": True
}
