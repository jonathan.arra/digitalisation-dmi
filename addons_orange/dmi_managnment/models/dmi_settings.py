# -*- encoding: utf-8 -*-

import time
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from datetime import date
from odoo import fields, models, api, _


class DMIType(models.Model):
    _name = "dmi.type"
    _description = "DMI Type"

    name = fields.Char("Nom", required=True)
    code = fields.Char("Code", required=True)
    description = fields.Text("Description")
    support_n1_id = fields.Many2one("hr.department", "Support N+1")
    support_n2_id = fields.Many2one("hr.department", "Support N+2")


class DMITypeApplication(models.Model):
    _name = "dmi.type.application"
    _description = "DMI Type Application"

    name = fields.Char("Nom", required=True)
    description = fields.Text("Description", required=False)
    code = fields.Char("Code")


class DMITypeApplicationProfil(models.Model):
    _name = "dmi.type.application.profil"
    _description = "DMI Type Application Profil"

    name = fields.Char("Nom", required=True)
    description = fields.Text("Description", required=False)
    application_id = fields.Many2one("dmi.type.application", "Application", required=True)
    code = fields.Char("Code")


class DMILineCategory(models.Model):
    _name = "dmi.line.category"
    _description = "DMI Line Category"

    name = fields.Char("Nom", required=True)
    description = fields.Text("Description", required=False)


class DMIOperationSystem(models.Model):
    _name = "dmi.operation.system"
    _description = "DMI Operation System"

    name = fields.Char("Nom", required=True)
    description = fields.Text("Description")


class DMITypeEquipment(models.Model):
    _name = "dmi.equipment.type.group"
    _description = "DMI Group Equipement"

    name = fields.Char("Nom", required=True)
    description = fields.Text("Description")
    equip_ids = fields.One2many("dmi.equipment.type", "group_id", "Equipements", required=False)
    mail_template_ids = fields.Many2many('mail.template', string='Mail Template')


class DMITypeEquipment(models.Model):
    _name = "dmi.equipment.type"
    _description = "DMI Type Equipement"

    name = fields.Char("Nom", required=True)
    description = fields.Text("Description")
    group_id = fields.Many2one("dmi.equipment.type.group", "Famille d'Equipement", required=True)



