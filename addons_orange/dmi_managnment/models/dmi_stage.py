# -*- coding: utf-8 -*-
# Part of Softhealer Technologies.

from odoo import models, fields


class DMIStages(models.Model):
    _name = 'dmi.stages'
    _description = "DMI Stages"
    _order = 'sequence ASC'
    _rec_name = 'name'

    name = fields.Char("Nom", required=True, translate=True)
    mail_template_ids = fields.Many2many('mail.template', string='Modèe de Mail')
    next_stage = fields.Many2one(comodel_name='dmi.stages', string='Etape suivante')
    previous_stage = fields.Many2one(comodel_name='dmi.stages', string='Etape Précédente')
    group_ids = fields.Many2many(comodel_name='res.groups', string='Groupe')
    is_cancel_button_visible = fields.Boolean(string='Is Cancel Button Visible ?')
    is_done_button_visible = fields.Boolean(string='Is Resolved Button Visible ?')
    sequence = fields.Integer(string="Séquence")
    state = fields.Selection([('draft', "Brouillon"), ('manager', "Manager"), ('senior', "Senior Manager"),
          ('sec_op', "Securite opérationnel"), ('manager_sec_oper', 'Manager Sécurité Op'), ('smc', "En cours traitement"),
          ('smc', "En cours traitement"), ('cancel', "Annulé"), ('done', "Validé"), ('rejet', "Rejeté"),
          ('info', "Demande d'informations")], string="Statut", default='draft')
    notification_ids = fields.One2many("dmi.stage.notification", "stage_id", "Notifications", required=False,
                                       ondelete='set Null')


class DMIStageNotification(models.Model):
    _name = "dmi.stage.notification"
    _description = "Gestion des notification par stage"
    _rec_name = "model_id"

    model_id = fields.Many2one("ir.model", "Modèle", required=False)
    model = fields.Char("Model", related="model_id.model")
    mails_ids = fields.Many2many("mail.template", string="Emails")
    stage_id = fields.Many2one("dmi.stages", "Etape", required=False)
    next_stage = fields.Many2one(comodel_name='dmi.stages', string='Etape suivante')
    previous_stage = fields.Many2one(comodel_name='dmi.stages', string='Etape Précédente')