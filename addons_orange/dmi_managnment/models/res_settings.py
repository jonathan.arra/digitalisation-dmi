# -*- coding: utf-8 -*-
# Part of Softhealer Technologies.

from odoo import models, fields, api


class ResCompany(models.Model):
    _inherit = 'res.company'

    new_stage_id = fields.Many2one("dmi.stages", "Etape de demarrage", required=False)
    done_stage_id = fields.Many2one("dmi.stages", "Etape de cloture", required=False)
    cancel_stage_id = fields.Many2one("dmi.stages", "Etape d'Annulation", required=False)
    sec_op_stage_id = fields.Many2one("dmi.stages", "Etape de validation Sécurite Operationnelle", required=False)
    deputy_director_of_operation_id = fields.Many2one("res.users", "Directeur Adjoint des opérations")
    manager_securty_op_id = fields.Many2one("res.users", "Manager Securité Operationelle")
    equi_manager_stage_id = fields.Many2one("dmi.stages", "Etape de validation Senoir Manager/Directeur")
    equi_do_stage_id = fields.Many2one("dmi.stages", "Etape de validation DO")
    infos_stage_id = fields.Many2one("dmi.stages", "Etape demande d'information")
    mail_dmi_notification = fields.Char("Mail de notification DMI", default="digitalisationdmis.oci@orange.com")
    mail_smc = fields.Char("Mail de notification DMI", default="smcit.oci@orange.com")


class DMISettings(models.TransientModel):
    _inherit = 'res.config.settings'

    new_stage_id = fields.Many2one("dmi.stages", "Etape de demarrage", required=False,
                                   related="company_id.new_stage_id", readonly=False, store=True)
    done_stage_id = fields.Many2one("dmi.stages", "Etape de cloture", required=False,
                                    related="company_id.done_stage_id", readonly=False, store=True)
    cancel_stage_id = fields.Many2one("dmi.stages", "Etape d'Annulation", required=False,
                                      related="company_id.cancel_stage_id", readonly=False, store=True)
    sec_op_stage_id = fields.Many2one("dmi.stages", "Etape de validation Security Operqtionnel", required=False,
                                      related="company_id.sec_op_stage_id", readonly=False, store=True)
    deputy_director_of_operation_id = fields.Many2one("res.users", "Directeur Adjoint des operations",
                        related="company_id.deputy_director_of_operation_id", readonly=False, store=True)
    equi_manager_stage_id = fields.Many2one("dmi.stages", "Etape de validation Senoir Manager/Directeur",
                required=False, related="company_id.equi_manager_stage_id", readonly=False, store=True)
    equi_do_stage_id = fields.Many2one("dmi.stages", "Etape de validation DO", required=False,
                                       related="company_id.equi_do_stage_id", readonly=False, store=True)
    manager_securty_op_id = fields.Many2one("res.users", "Manager Security Operations",
                                            related="company_id.manager_securty_op_id", readonly=False, store=True)
    infos_stage_id = fields.Many2one("dmi.stages", "Etape de demande d'information", required=False,
                                       related="company_id.infos_stage_id", readonly=False, store=True)
    mail_dmi_notification = fields.Char("Mail de notification DMI", related="company_id.mail_dmi_notification",
                                        readonly=False, store=True)
    mail_smc = fields.Char("Mail de notification DMI", related="company_id.mail_smc",
                                        readonly=False, store=True)