# -*- encoding: utf-8 -*-

import time
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from datetime import date
from odoo import fields, models, api, _, SUPERUSER_ID


READONLY = {
                'cancel': [('readonly', True)], 'done': [('readonly', True)],
                'reject': [('readonly', True)], 'manager': [('readonly', True)],
                'senior': [('readonly', True)]
            }


class DMIManagnmentHabilitiation(models.Model):
    _name = "dmi.managnment.habilitation"
    _inherit = ['dmi.template', 'mail.thread', 'mail.activity.mixin']
    _description = "DMI Managnment Habilitation"

    type = fields.Selection([('creation', "Création"), ('update', "Modification"), ("delete", "Suppression")],
                            string="Type de la demande", default='création')
    type_document = fields.Char("Code document", default="HAB")
    state = fields.Selection([('draft', "Initialisation"), ('manager', "En attente de validation"),
              ('senior', "En attente de validation"), ('smc', "En cours traitement"), ('cancel', "Annulé"),
              ('done', "Cloturé"), ('rejet', "Rejeté"), ('info', "En attente d'informations complémentaires")],
                string="Statut", default='draft')
    line_ids = fields.One2many("dmi.managnment.habilitation.line", "habilitation_id", "Lignes", required=True)
    line_system_ids = fields.One2many("dmi.managnment.habilitation.line", "habilitation_id", "Lignes", required=True)
    type_management = fields.Selection([('om', "Orange Money"), ('app', "Applications/Services"),
                                        ('other', "Compte systeme et BD")], "Type de DMI", default='app')
    desired_date = fields.Date("Date d'implementation souhaitée")
    infos_ids = fields.One2many("dmi.infos.line", "res_id", string="Demandes D'informations",
                                domain=[('res_model', '=', 'dmi.managnment.habilitation')])

    @api.onchange('type')
    def onChangeType(self):
        self.ensure_one()
        if self.type == "delete":
            self.validity = "permanent"

    def action_to_draft(self):
        for res in self:
            res.stage_id = res.company_id.new_stage_id
            res.state = res.company_id.new_stage_id.state

    @api.model
    def create(self, vals):
        self = self.with_company(vals['company_id'])
        vals['name'] = self.env['ir.sequence'].next_by_code("dmi.managnment.habilitation") or _('New')
        return super(DMIManagnmentHabilitiation, self).create(vals)


class DMIManagnmentHabilitiationLine(models.Model):
    _name = "dmi.managnment.habilitation.line"
    _description = "DMI Managnment Habilitation Line"

    server_name = fields.Char("serveur", required=False)
    ip_server = fields.Char("Adresse IP", required=False)
    right_request = fields.Char("Droits souhaité", required=False)
    application_id = fields.Many2one("dmi.type.application", "Application", required=False)
    old_profil_id = fields.Many2one("dmi.type.application.profil", "Profil Actuel", required=False,
                                    domain="[('application_id','=', application_id),('id', '!=', new_profil_id)]")
    new_profil_id = fields.Many2one("dmi.type.application.profil", "Profil demande", required=False,
                                    domain="[('application_id','=', application_id),('id', '!=', old_profil_id)]")
    validity = fields.Selection([('permanent', "permanent"), ('tempory', "Temporaire")],
                                default="tempory", string="Validite")
    validity_periode_start = fields.Date("Du", required=False)
    validity_periode_end = fields.Date("Au", required=False)
    intervale_time = fields.Selection([('full_day', "Journee entiere"), ('other', "Autre")], "Plage horaire",
                                      default="full_day", states=READONLY)
    hour_start = fields.Char("De", states=READONLY)
    hour_end = fields.Char("A", states=READONLY)
    habilitation_id = fields.Many2one("dmi.managnment.habilitation", "DMI", required=False)
    project_id = fields.Many2one("project.project", "projet", required=False, related="habilitation_id.project_id")
    type_management = fields.Selection([('om', "Orange Money"), ('hab', "Habilitations"),
                        ('other', "Compte système et BD")], "Type de DMI", related='habilitation_id.type_management')
    type = fields.Selection([('creation', "Création"), ('update', "Modification"), ("delete", "Suppression")],
                            string="Type de la demande", default='creation', related='habilitation_id.type')

