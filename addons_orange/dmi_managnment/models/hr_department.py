# -*- encoding: utf-8 -*-

import time
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from datetime import date
from odoo import fields, models, api, _


class HrDepartment(models.Model):
    _inherit = "hr.department"

    type = fields.Selection([('direction', "Direction"), ('department', "Departement"), ('service', "Service")],
                            default=False, string="Type")