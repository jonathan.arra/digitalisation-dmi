# -*- encoding: utf-8 -*-

import time
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from datetime import date
from odoo import fields, models, api, _, SUPERUSER_ID


READONLY = {
                'cancel': [('readonly', True)], 'done': [('readonly', True)],
                'reject': [('readonly', True)], 'manager': [('readonly', True)],
                'senior': [('readonly', True)]
            }


class DMIManagnmentEquipment(models.Model):
    _name = "dmi.managnment.equipment"
    _inherit = ['dmi.template', 'mail.thread', 'mail.activity.mixin']
    _description = "DMI Managnment Equipement"

    owner_id = fields.Many2one("hr.department", "Propriétaire", required=False, tracking=True)
    type = fields.Selection([('creation', "Création"), ('update', "Modification")], string="Type de la demande",
                            default='création')
    type_document = fields.Char("Code document", default="EQ")
    state = fields.Selection([('draft', "Brouillon"), ('manager', "En attente de validation"), ('senior', "En attente de validation"),
              ('smc', "En cours traitement"), ('cancel', "Annulé"), ('done', "Validé"), ('info', "En attente d'informations complementaires"),
              ('rejet', "Rejeté")], string="Statut", default='draft')
    desired_date = fields.Date("Date d'installation souhaitée", required=True)
    site_id = fields.Many2one("hr.work.location", "Site du demadeur", required=True)
    validity = fields.Selection([('permanent', "permanent"), ('tempory', "Temporaire")],
                                default="tempory", string="Validité")
    line_ids = fields.One2many("dmi.managnment.equipment.line", "dmi_id", "Lines", required=True)
    infos_ids = fields.One2many("dmi.infos.line", "res_id", string="Demandes D'informations",
                                domain=[('res_model', '=', 'dmi.managnment.equipment')])

    @api.model
    def create(self, vals):
        self = self.with_company(vals['company_id'])
        vals['name'] = self.env['ir.sequence'].next_by_code("dmi.managnment.equipment") or _('New')
        return super(DMIManagnmentEquipment, self).create(vals)


class DMIManagnmentEquipmentLine(models.Model):
    _name = "dmi.managnment.equipment.line"
    _description = "Line Equipement"

    site_id = fields.Many2one("hr.work.location", "Site du demadeur", required=True)
    equipment_id = fields.Many2one("dmi.equipment.type", "Equipement", required=True)
    group_equipment_id = fields.Many2one("dmi.equipment.type.group", "Equipement", related="equipment_id.group_id",
                                         readonly=True)
    validity = fields.Selection([('permanent', "permanent"), ('tempory', "Temporaire")], string="Validité",
                                related="dmi_id.validity", store=True)
    validity_periode_start = fields.Date("Du", required=False)
    validity_periode_end = fields.Date("Au", required=False)
    intervale_time = fields.Selection([('full_day', "Journee entière"), ('other', "Autre")], "Plage horaire",
                                      default="full_day")
    hour_start = fields.Char("DU")
    hour_end = fields.Char("AU")
    dmi_id = fields.Many2one("dmi.managnment.equipment", "DMI", required=False)

