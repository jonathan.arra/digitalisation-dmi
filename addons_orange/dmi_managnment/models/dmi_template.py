# -*- encoding: utf-8 -*-

import time
import logging
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from datetime import date
from odoo import fields, models, api, _, SUPERUSER_ID
from odoo.http import request

_logger = logging.getLogger(__name__)


READONLY = {
    'cancel': [('readonly', True)], 'done': [('readonly', True)], 'reject': [('readonly', True)],
    'manager': [('readonly', True)], 'senior': [('readonly', True)], 'sec_op': [('readonly', True)],
    'manager_sec_op': [('readonly', True)]
}


class DMITemplate(models.Model):
    _name = "dmi.template"
    _description = "Patron de conception DMI"

    @api.model
    def get_default_stage(self):
        company_id = self.env.company
        stage_id = self.env['dmi.stages'].sudo().search(
            [('id', '=', company_id.new_stage_id.id)], limit=1)
        print(stage_id.name)
        return stage_id.id

    def get_default_company(self):
        company_id = self.env.company
        return company_id

    # @api.onchange('type_document')
    # def onChangeType_document(self):
    #     self.ensure_one()
    #     if self.type_document:
    #         type = self.env["dmi.type"].search([('code', '=', self.type_document)], limit=1)
    #         print(type.name)
    #         if type:
    #             self.type_id = type
    #     return False

    @api.model
    def _read_group_stage_ids(self, stages, domain, order):
        all_stages = self.env['dmi.stages'].sudo().search([])
        search_domain = [('id', 'in', all_stages.ids)]

        # perform search
        stage_ids = stages._search(search_domain,
                                   order=order,
                                   access_rights_uid=SUPERUSER_ID)
        return stages.browse(stage_ids)

    @api.depends("user_id")
    def _get_department_owner(self):
        self.ensure_one()
        self.department_user_id = False
        employee = self.env["hr.employee"].search([('user_id', '=', self.user_id.id)])
        if employee and employee.parent_id and employee.parent_id.department_id:
            self.department_user_id = employee.parent_id.department_id

    @api.depends("beneficiary_user_id")
    def _get_department_beneficiary(self):
        self.ensure_one()
        self.department_beneficiary_id = False
        if self.beneficiary_user_id:
            employee = self.env["hr.employee"].search([('user_id', '=', self.beneficiary_user_id.id)])
            if employee and employee.department_id:
                self.department_beneficiary_id = employee.department_id


    def _get_Manager_id(self):
        self.ensure_one()
        employee = self.env['hr.employee'].search([('user_id', '=', self.user_id.id)])
        print(employee.name)
        if employee and employee.parent_id:
            if employee.parent_id.user_id:
                return employee.parent_id.user_id.id
        return False

    @api.depends("manager_id")
    def _get_Senior_Manager_id(self):
        self.ensure_one()
        employee = self.env["hr.employee"].search([('user_id', '=', self.user_id.id)])
        if employee and employee.parent_id:
            if employee.parent_id.user_id:
                return employee.parent_id.user_id.id
        return False

    @api.onchange("user_id")
    def _get_all_user_infos(self):
        self.ensure_one()
        if self.user_id:
            employee = self.env["hr.employee"].search([('user_id', '=', self.user_id.id)], limit=1)
            manager_id = employee.parent_id
            senior_manager_id = manager_id.parent_id
            if employee and manager_id and manager_id.user_id:
                self.manager_id = manager_id.user_id
            if manager_id and senior_manager_id.user_id:
                self.senior_manager_id = senior_manager_id.user_id

    @api.onchange("type_beneficiarie")
    def onChangeTypeBeneficiaire(self):
        if self.type_beneficiarie == 'interne':
            self.partner_id = False
            self.telephone = self.beneficiary_user_id.name
            self.email = ""
        elif self.type_beneficiarie == 'externe':
            self.beneficiary_user_id = False
            self.telephone = self.beneficiary_user_id.name
            self.email = ""
        else:
            self.partner_id = False
            self.beneficiary_user_id = False
            self.telephone = False
            self.email = False

    name = fields.Char("Référence", required=True, default="/", tracking=True)
    user_id = fields.Many2one("res.users", "Demandeur", required=True, default=lambda self: self.env.user.id,
                              tracking=True)
    department_user_id = fields.Many2one("hr.department", "Departement du demandeur", store=True,
                                         compute="_get_department_owner")
    type_beneficiarie = fields.Selection([('interne', 'Interne'), ('supplier', "Fournisseur")], default="interne",
                                         string="Type de beneficiaire")
    beneficiary_user_id = fields.Many2one("res.users", "Bénéficiaire", required=False,
                                          default=lambda self: self.env.user.id, tracking=True)
    partner_id = fields.Many2one("res.partner", "Fournisseur", required=False, domain="[('type_partner', '=', 'partner')]")
    telephone = fields.Char("Contact téléphonique",  required=False)
    email = fields.Char("Email", required=False)
    department_beneficiary_id = fields.Many2one("hr.department", "Departement du Bénéficiaire", store=True,
                                                compute="_get_department_beneficiary")
    manager_id = fields.Many2one("res.users", "Manager")
    senior_manager_id = fields.Many2one("res.users", "Senior Manager")
    type_id = fields.Many2one("dmi.type", "Type", required=False, tracking=True)
    old_stage_id = fields.Many2one('dmi.stages', string="Etape", tracking=True, index=True, copy=False)
    stage_id = fields.Many2one('dmi.stages', string="Etape", default=lambda self: self.env.company.new_stage_id.id,
                               tracking=True, index=True, copy=False, group_expand='_read_group_stage_ids')
    company_id = fields.Many2one('res.company', string="Société", default=lambda self: self.env.company.id)
    type_document = fields.Char("Code document", default="FLUX")
    state = fields.Selection([('draft', "Initialisation"), ('manager', "En attente de validation"),
                  ('senior', "En attente de validation"), ('sec_op', "En attente de validation"),
                  ('manager_sec_oper', "En attente de validation"), ('smc', "En cours traitement"), ('cancel', "Annule"), ('done', "Cloture"),
                              ('rejet', "Rejete")], default="draft",
                             string="Statut")
    type = fields.Selection([('creation', "Création"), ('update', "Modification"), ("delete", "Suppression")],
                            string="Type de la demande", default='creation')
    contact_id = fields.Many2one("hr.employee", "Contact en cas de probleme", required=True)
    user_cancel_id = fields.Many2one("res.users", "Refusé par")
    motif_refus = fields.Text("Motif de refus")
    comment = fields.Text("Commentaire", required=True)
    demande_type = fields.Selection([('interne', "Interne"), ('project', "Lie à un projet")], default="interne",
                                    string="Type de demande")
    project_id = fields.Many2one("project.project", "projet", required=False)
    validator_id = fields.Many2one("res.users", "Validateur", required=False)
    in_charge_of_id = fields.Many2one("res.users", "En charge de la DMI", required=False)
    question = fields.Boolean("Question", default=False)
    numero_ticket = fields.Char('# ticket GLPI')
    record_url = fields.Char("URL", compute='_compute_record_url')

    def _get_smc_diffusion(self):
        """ This method is call when notify smc team
        Returns: Emails list
        """
        ir_model_data = self.env['ir.model.data']
        group_obj = self.env['res.groups']
        email_list = []

        insurance_users_model_id = ir_model_data._xmlid_lookup('dmi_managnment.group_incident_reporting_manager')[2]
        insurance_users_group_id = group_obj.browse(insurance_users_model_id)

        insurance_manager_model_id = \
        ir_model_data._xmlid_lookup('incident_reporting.group_incident_reporting_insurance_manager')[2]
        insurance_manager_group_id = group_obj.browse(insurance_manager_model_id)

        if insurance_users_group_id.users:
            for e in insurance_users_group_id.users.employee_id.mapped('work_email'):
                email_list.append(e)

        if insurance_manager_group_id.users:
            for e in insurance_manager_group_id.users.employee_id.mapped('work_email'):
                email_list.append(e)

        return ",".join([e for e in email_list if e])

    @api.model
    def _compute_record_url(self):
        """ This method is called for computed current record url.
        Returns: Current record url
        """
        menu = self.env.ref('incident_reporting.menu_root')
        base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url')
        base_url += '/web#id=%d&view_type=form&model=%s&menu_id=%s' % (self.id, self._name, menu.id)
        self.record_url = base_url

    def _get_stage_step(self, stage):
        step_stage_id = self.env['dmi.stage.notification'].search([('model', '=', self._name),
                                                                   ('stage_id', '=', stage.id)])
        if step_stage_id:
            return step_stage_id
        else:
            return False

    def action_send_to_manager(self):
        for res in self:
            _logger.warning(self._name)
            res.old_stage_id = res.stage_id
            step_stage = self._get_stage_step(res.stage_id)
            if step_stage:
                _logger.warning(step_stage)
                if step_stage.next_stage:
                    next_stage = step_stage.next_stage
                    res.stage_id = next_stage
                    res.state = next_stage.state
                    res.validator_id = res.manager_id
                    if step_stage.mails_ids:
                        for template in step_stage.mails_ids:
                            template.sudo().send_mail(res.id, force_send=True)

    def action_confirm_send_to_senior(self):
        for res in self:
            res.old_stage_id = res.stage_id
            step_stage = self._get_stage_step(res.stage_id)
            if step_stage:
                _logger.warning(step_stage)
                if step_stage.next_stage:
                    next_stage = step_stage.next_stage
                    res.stage_id = next_stage
                    res.state = next_stage.state
                    res.validator_id = res.senior_manager_id
                    if step_stage.mails_ids:
                        for template in step_stage.mails_ids:
                            template.sudo().send_mail(res.id, force_send=True)

    def action_refused(self):
        for res in self:
            res.old_stage_id = res.stage_id
            res.stage_id = res.company_id.cancel_stage_id
            res.state = res.stage_id.state
            res.user_cancel_id = self.env.user
            step_stage = self._get_stage_step(res.stage_id)
            if step_stage and step_stage.mails_ids:
                for template in step_stage.mails_ids:
                    template.sudo().send_mail(res.id, force_send=True)

    def action_done(self):
        for res in self:
            res.old_stage_id = res.stage_id
            res.stage_id = res.company_id.done_stage_id
            res.state = res.stage_id.state
            step_stage = self._get_stage_step(res.stage_id)
            if step_stage and step_stage.mails_ids:
                for template in step_stage.mails_ids:
                    template.sudo().send_mail(res.id, force_send=True)

    def action_cancel(self):
        for res in self:
            res.old_stage_id = res.stage_id
            res.stage_id = res.company_id.cancel_stage_id
            res.state = res.stage_id.state
            step_stage = self._get_stage_step(res.stage_id)
            if step_stage and step_stage.mails_ids:
                for template in step_stage.mails_ids:
                    template.sudo().send_mail(res.id, force_send=True)

    def action_confirm_send_to_smc(self):
        for res in self:
            res.old_stage_id = res.stage_id
            step_stage = self._get_stage_step(res.stage_id)
            if step_stage:
                # logging.waring()
                res.state = step_stage.next_stage.state
                res.validator_id = res.senior_manager_id
                if step_stage.mails_ids:
                    for template in step_stage.mails_ids:
                        template.sudo().send_mail(res.id, force_send=True)

    def action_ask_more_infos(self):
        for res in self:
            step_stage = res.company_id.infos_stage_id
            res.old_stage_id = res.stage_id
            if step_stage:
                stage = self._get_stage_step(step_stage)
                res.stage_id = step_stage
                res.state = step_stage.state
                # res.validator_id = res.senior_manager_id
                if stage and stage.mails_ids:
                    for template in stage.mails_ids:
                        template.sudo().send_mail(res.id, force_send=True)

    def action_send_answer(self):
        for res in self:
            res.old_stage_id = res.stage_id
            step_stage = res.company_id.infos_stage_id
            if step_stage:
                stage = self._get_stage_step(step_stage)
                if stage:
                    next_stage = stage.next_stage
                    res.stage_id = next_stage
                    res.state = next_stage.state
                    # res.validator_id = res.senior_manager_id
                    if stage.mails_ids:
                        for template in stage.mails_ids:
                            template.sudo().send_mail(res.id, force_send=True)

    def action_to_draft(self):
        for res in self:
            res.stage_id = res.company_id.new_stage_id
            res.state = res.company_id.new_stage_id.state


class DMITimeManagement(models.Model):
    _name = "dmi.time.management"
    _description = "DMI time management"

    start_date = fields.Datetime("DEBUT", required=True)
    end_date = fields.Datetime("FIN", required=True)
    stage_id = fields.Many2one('dmi.stages', string="Etape")
    time = fields.Float("DUREE")
    model = fields.Char("Model", required=True)
    res_id = fields.Integer("ID", required=True)


class DMIInfosManagement(models.Model):
    _name = "dmi.infos.line"
    _description = "DMI infos Management"

    start_date = fields.Datetime("DATE DEMANDE INFORMATION", required=True)
    end_date = fields.Datetime("DATE REPONSE", required=False)
    infos_comment = fields.Char(string="REQUETE")
    response_comment = fields.Char("REPONSE")
    res_model = fields.Char("MODEL", required=True)
    res_id = fields.Integer("ID", required=True)
    state = fields.Selection([('en_attente', "En attente de reponse"), ('repondu', "Repondu")], string="STATUT",
                             default="en_attente")
    user_id = fields.Many2one("res.users", "DEMANDEUR", required=True)
    answer_user_id = fields.Many2one("res.users", "Reponse apportee par", required=False)

# class DMIValidationLine(models.Model):
#     _name = "dmi.validation.line"
#     _description = "Validation Management System"
#
#     user_id = fields.Many2one("res.users", "Validataire", required)
#     model = fields.char("Model", required=True)



