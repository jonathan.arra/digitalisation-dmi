# -*- encoding: utf-8 -*-

import time
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from datetime import date
from odoo import fields, models, api, _, SUPERUSER_ID


READONLY = {
                'cancel': [('readonly', True)], 'done': [('readonly', True)],
                'reject': [('readonly', True)], 'manager': [('readonly', True)],
                'senior': [('readonly', True)]
            }


class DMIManagnment(models.Model):
    _name = "dmi.managnment"
    _inherit = ['dmi.template', 'mail.thread', 'mail.activity.mixin']
    _description = "DMI VMs Managnment"

    owner_id = fields.Many2one("hr.department", "Propriétaire", required=False, tracking=True)
    line_ids = fields.One2many("dmi.managnment.line", "dmi_id", "Lignes")
    enabled_by_vpn = fields.Boolean("Acces VPN")
    internet_enable = fields.Boolean("Acces Internet")
    user_cancel_id = fields.Many2one("res.users", "Refusé par")
    motif_refus = fields.Text("Motif de refus")
    state = fields.Selection([('draft', "Brouillon"), ('manager', "En cours de validation"), ('senior', "En cours de validation"),
                  ('info', "En attente d'informations complementaires"),  ('smc', "En cours traitement"), ('cancel', "Annule"),
                  ('done', "Cloturer"), ('rejet', "Rejete")], default="draft", string="Statut")
    validator_id = fields.Many2one("res.users", "Valideur", required=False)
    in_charge_of_id = fields.Many2one("res.users", "En charge du DMI", required=False)
    ip_file = fields.Binary("Fichier des IPs", required=True)
    infos_ids = fields.One2many("dmi.infos.line", "res_id", string="Demandes D'informations",
                                domain=[('res_model', '=', 'dmi.managnment')])

    @api.model
    def create(self, vals):
        self = self.with_company(vals['company_id'])
        vals['name'] = self.env['ir.sequence'].next_by_code("dmi.managnment.vm") or _('New')
        return super(DMIManagnment, self).create(vals)


class DMIManagnmentLine(models.Model):
    _name = "dmi.managnment.line"
    _description = "DMI Managnment Line"

    name = fields.Char("Nom VM")
    category_ids = fields.Many2many("dmi.line.category", "dmi_category_real", "line_id", "category_id", "Categories")
    os_id = fields.Many2one("dmi.operation.system", "Systeme d'exploitation", required=True)
    nb_cpu = fields.Integer("NB CPUs")
    ram = fields.Integer("RAM en Go")
    hard_disque = fields.Integer("Disque (Partitionnement)")
    observation = fields.Text("Observations")
    dmi_id = fields.Many2one("dmi.managnment", "DMI", required=False)
