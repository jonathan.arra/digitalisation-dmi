# -*- encoding: utf-8 -*-

import time
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from datetime import date
from odoo import fields, models, api, _


class ResPartner(models.Model):
    _inherit = "res.partner"

    type_partner = fields.Selection([('partner', "Partenaire"), ('employe', "Employé")],
                                    string="Type de partenaire", default=False)