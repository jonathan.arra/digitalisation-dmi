# -*- encoding: utf-8 -*-

import time
import logging
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from datetime import date
from odoo import fields, models, api, _, SUPERUSER_ID

_logger = logging.getLogger(__name__)


# class DMITypeFlux(models.Model):
#     _name = "dmi.type.flux"
#     _description = "Management des types de flux"
#
#     name = fields.Char("Libelle", required=True)
READONLY = {
    'cancel': [('readonly', True)], 'done': [('readonly', True)],'reject': [('readonly', True)],
    'manager': [('readonly', True)], 'senior': [('readonly', True)], 'sec_op': [('readonly', True)],
    'manager_sec_op': [('readonly', True)]
 }


class DMIManagnmentFlux(models.Model):
    _name = "dmi.managnment.flux"
    _inherit = ['dmi.template', 'mail.thread', 'mail.activity.mixin']
    _description = "DMI Ouverture de flux"

    line_ids = fields.One2many("dmi.managnment.flux.line", "dmi_id", "Lignes", required=True)

    validity = fields.Selection([('permanent', "Permanent"), ('tempory', "Temporaire")],
                                default="tempory", string="Validité")
    validity_periode_start = fields.Date("Du", required=False)
    validity_periode_end = fields.Date("Au", required=False)
    intervale_time = fields.Selection([('full_day', "Journee entière"), ('other', "Autre")], "Plage horaire",
                                      default="full_day")
    hour_start = fields.Char("De")
    hour_end = fields.Char("A")
    type_flux = fields.Selection([('l2l', "Lan to Lan"), ('interne', "Plateforme Interne"),
                                  ('remote', "Remote Access")], "Type de flux", default="False")
    infos_ids = fields.One2many("dmi.infos.line", "res_id", string="Demandes D'informations",
                                domain=[('res_model', '=', 'dmi.managnment.habilitation')])

    def action_confirm_send_to_sec_op(self):
        for res in self:
            step_stage = self._get_stage_step(res.stage_id)
            if step_stage:
                _logger.warning(step_stage)
                if step_stage.next_stage:
                    next_stage = step_stage.next_stage
                    res.stage_id = next_stage
                    res.state = next_stage.state
                    if step_stage.mails_ids:
                        for template in step_stage.mails_ids:
                            template.sudo().send_mail(res.id, force_send=True)

    def action_confirm_send_to_manager_sec_operation(self):
        for res in self:
            step_stage = self._get_stage_step(res.stage_id)
            if step_stage:
                _logger.warning(step_stage)
                if step_stage.next_stage:
                    next_stage = step_stage.next_stage
                    res.stage_id = next_stage
                    res.state = next_stage.state
                    res.validator_id = res.company_id.manager_securty_op_id
                    if step_stage.mails_ids:
                        for template in step_stage.mails_ids:
                            template.sudo().send_mail(res.id, force_send=True)

    def action_confirm_manager_sec_operation(self):
        for res in self:
            step_stage = self._get_stage_step(res.stage_id)
            if step_stage:
                _logger.warning(step_stage)
                if step_stage.next_stage:
                    next_stage = step_stage.next_stage
                    res.stage_id = next_stage
                    res.state = next_stage.state
                    if step_stage.mails_ids:
                        for template in step_stage.mails_ids:
                            template.sudo().send_mail(res.id, force_send=True)

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code("dmi.managnment.flux") or _('New')
        return super(DMIManagnmentFlux, self).create(vals)


class DMIManagnmentLine(models.Model):
    _name = "dmi.managnment.flux.line"
    _description = "DMI Management Flux Line"

    source_ip = fields.Char("IP Address/Mask", required=True)
    source_description = fields.Char("Description", required=True)
    dest_ip = fields.Char("IP Address/Mask", required=True)
    dest_description = fields.Char("Description", required=True)
    protocole = fields.Char("Protocole")
    port = fields.Integer("Port")
    dest_ip_nat = fields.Char("Destination NAT")
    dmi_id = fields.Many2one("dmi.managnment.flux", "DMI", required=False)
