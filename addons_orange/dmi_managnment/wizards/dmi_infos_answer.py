# -*- coding: utf-8 -*-
# Part of Softhealer Technologies.

from odoo import api, fields, models, tools, _
import re
from odoo.exceptions import UserError
# import html2text


class DMIRefusManagnment(models.TransientModel):
    _name = "dmi.more.information.answer_wizard"
    _description = 'Demande dinformation'

    @api.model
    def _getDefaultDMI(self):
        print(self.env.context)
        active_model = self.env.context.get("active_model")
        ct_active_id = self.env.context.get("active_id")
        ticket = self.env[active_model].browse(ct_active_id)
        if ticket:
            return ticket.id
        return False

    @api.model
    def _getDefaultQuestion(self):
        active_model = self.env.context.get("active_model")
        ct_active_id = self.env.context.get("active_id")
        ticket = self.env[active_model].browse(ct_active_id)
        if ticket:
            question = ticket.infos_ids.filtered(lambda i: i.state == 'en_attente')[0]
            if question:
                print(question)
                return question.id
        return False

    comment = fields.Text("Reponse")
    user_id = fields.Many2one("res.users", "Utilisateur", required=True, default=lambda self: self.env.user.id)
    dmi_id = fields.Many2one("dmi.managnment", "DMI", required=False, default=_getDefaultDMI)
    info_id = fields.Many2one('dmi.infos.line', "Question", required=True, default=_getDefaultQuestion)

    def action_send_answer(self):
        for rec in self:
            if rec.info_id:
                vals = {
                    "response_comment": rec.comment,
                    "state": 'repondu',
                    "answer_user_id": self.env.user.id,
                    "end_date": fields.Datetime.now()
                }
                rec.info_id.write(vals)
                rec.dmi_id.action_send_answer()
        return True
