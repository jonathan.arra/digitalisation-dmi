# -*- coding: utf-8 -*-
# Part of Softhealer Technologies.

from odoo import api, fields, models, tools, _
import re
from odoo.exceptions import UserError
# import html2text


class DMIRefusManagnment(models.TransientModel):
    _name = "dmi.refused.managnment"
    _description = 'refus managnment'

    @api.model
    def _getDefaultDMI(self):
        print(self.env.context)
        active_model = self.env.context.get("active_model")
        ct_active_id = self.env.context.get("active_id")
        ticket = self.env[active_model].browse(ct_active_id)
        if ticket:
            return ticket.id
        return False

    comment = fields.Text("Commentaire")
    user_id = fields.Many2one("res.users", "Utilisateur", required=True, default=lambda self: self.env.user.id)
    dmi_id = fields.Many2one("dmi.managnment", "DMI", required=False, default=_getDefaultDMI)

    def action_refus(self):
        for rec in self:
            if rec.dmi_id:
                rec.dmi_id.motif_refus = rec.comment
                rec.dmi_id.action_refused()
        return True
