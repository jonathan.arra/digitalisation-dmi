# -*- coding: utf-8 -*-
# Copyright 2016 Openworx, LasLabs Inc.
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

{
    "name": "Customisation ORANGE",
    "summary": "",
    "version": "16.1",
    "category": "Customisation",
    "author": "Jean-Jonathan ARRA",
    "license": "LGPL-3",
    "installable": True,
    "depends": [
        'project',
        'web'
    ],
    "data": [
    ],
    "assets": {
        "web.assets_backend": [
            'orange_custom/static/src/legacy/scss/navbar.scss'
        ]
    }
}

